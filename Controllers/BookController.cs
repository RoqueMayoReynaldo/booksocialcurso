﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookSocial2.DB;
using BookSocial2.Models;
namespace BookSocial2.Controllers
{
    public class libroComentario{
        public Libro book { get; set; }
        public List<Comentario> coments { get; set; }
        }
public class BookController : Controller
    {
       
        public AppBookSocial2Context context;
        public BookController(AppBookSocial2Context context)
        {
            this.context = context;
        }
        [HttpGet, HttpPost]
        public ViewResult Detalle(int id)
        {
           
            libroComentario lc = new libroComentario
            {
                book = context.Libros.FirstOrDefault(Libro => Libro.id == id),
                coments = context.Comentarios.Where(Comentario => Comentario.idBook == id).ToList()

            };

            return View("Detalle",lc);
        }
        [HttpGet,HttpPost]
        public IActionResult Addcomentario(Comentario comentario)
        {

            //validando el contenido del comentario ,haciendo uso de la propiedad ModelState

            var objetoAnonim = new { id = comentario.idBook };
            //if (String.IsNullOrEmpty(comentario.contenido))
                //{
                //    ModelState.AddModelError("validacion", "Porfavor ingresa un comentario");
                //}

                //if (!String.IsNullOrEmpty(comentario.contenido) && comentario.contenido.Length <5)
                //{
                //        ModelState.AddModelError("validacion", "Porfavor tu comentario debe contener almenos 5 caracteres");
                //}


                //validando que el ultimo comentario sea igual al comentario ingresado

            //    Comentario ultimoComent = context.Comentarios.OrderBy(Comentario=>Comentario.id).Last();
            //if (ultimoComent.contenido!=comentario.contenido)
            //{
            //    ModelState.AddModelError("contenido","Debe ser igual al anterior comentario");
            //}

            if (ModelState.IsValid) {

                context.Comentarios.Add(comentario);
                context.SaveChanges();
                return RedirectToAction("Detalle", objetoAnonim);
            }

          
                return Detalle(comentario.idBook);
            
        }

       
      
    }
}
