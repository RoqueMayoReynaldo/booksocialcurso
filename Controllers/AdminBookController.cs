﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookSocial2.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using BookSocial2.DB;
namespace BookSocial2.Controllers
{
    public class AdminBookController : Controller
    {
        //variable y constructor que permite calcular la ruta para guardar archivos y evitar poner c:/..../.../webroot/
        private IWebHostEnvironment hosting;
        private AppBookSocial2Context context;
        public AdminBookController(AppBookSocial2Context context,IWebHostEnvironment hosting)
        {
            this.hosting = hosting;
            this.context = context;
        }
        [HttpGet]
     
        public IActionResult Create()
        {
            return View();
        }
        // GET: AdminBookController/Create
        [HttpPost]
        public IActionResult Create(Libro libro,IFormFile file)
        {
            
          
            
            libro.portada = saveFile(file);
            

            context.Libros.Add(libro);
            context.SaveChanges();
            return RedirectToAction("Create");
        }
        [HttpGet]

        public IActionResult Editar(int id)
        {
            Libro book = context.Libros.Find(id);

            return View(book);
        }
        // GET: AdminBookController/Create
        [HttpPost]
        public IActionResult Editar(Libro libro, IFormFile file)
        {


            //tenemos el metood update para actualizar ,pero este nos haria perder datos si esque no se enviaran todos los datos a editar completamente
            //context.Libros.Update(libro)
            //pero podemos tenemos otra forma de hacerlo

            Libro book = context.Libros.Find(libro.id);
            book.nombre = libro.nombre;
            book.idAutor = libro.idAutor;

            context.SaveChanges();
            return RedirectToAction("Index");
        }
        public IActionResult Eliminar(int id)
        {

            Libro book = context.Libros.Find(id);
            context.Libros.Remove(book);
            context.SaveChanges();
            return RedirectToAction("Index");
        }
        private string saveFile(IFormFile file)
        {

            // string ruta = @"C:\Users\FUCK\source\repos\" + file.FileName;

            //hosting.webROOTPATH con estO,OBTENEMOS LA ruta PARA GUARDAR en wwwroot los archivos 
            //pathCombine nos permite el guardado en diferentes plataformas sin tener que especificar / o \ ,se separan con comas
            string relativePath = null;
            if (file.Length > 0 && file.ContentType=="image/png")
            {
                relativePath =@"\files\"+ file.FileName;
                string ruta = Path.Combine(hosting.WebRootPath,"files", file.FileName);
                FileStream stream = new FileStream(ruta, FileMode.Create);
                file.CopyTo(stream);
                stream.Close();
             
            }
            return relativePath.Replace("\\","/") ;
        }


     
    
    }
}
