﻿using BookSocial2.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BookSocial2.DB;
using Microsoft.EntityFrameworkCore;

namespace BookSocial2.Controllers
{
    public class HomeController : Controller
    {
        private AppBookSocial2Context context;
        public HomeController(AppBookSocial2Context context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            //usaremos modo string para llamar los datos de las entidades relacionadas
            var books = context.Libros.Include("bookAutores.autor").ToList();
            return View(books);
        }
       
    }
}
