﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BookSocial2.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookSocial2.DB.Mapping
{
    public class book_autorMap : IEntityTypeConfiguration<book_autor>
    {
        public void Configure(EntityTypeBuilder<book_autor> builder)
        {
            builder.ToTable("book_autor", "dbo");
            builder.HasKey(book_autor => book_autor.id);
            builder.HasOne(book_autor => book_autor.autor).WithMany().HasForeignKey(book_autor => book_autor.idAutor);
        }

    }
}
