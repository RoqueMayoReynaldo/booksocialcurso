﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using BookSocial2.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BookSocial2.DB.Mapping
{
    public class AutorMap : IEntityTypeConfiguration<Autor>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<Autor> builder)
        {
            builder.ToTable("Autor", "dbo");
            builder.HasKey(Autor =>Autor.id);
        }
    }
}
