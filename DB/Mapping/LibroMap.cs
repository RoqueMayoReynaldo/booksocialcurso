﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookSocial2.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace BookSocial2.DB.Mapping
{
    public class LibroMap : IEntityTypeConfiguration<Libro>
    {
        public void Configure(EntityTypeBuilder<Libro> builder)
        {
            //builder.ToTable("Book","dbo");
            //builder.HasKey(Libro=>Libro.id);
            //builder.HasOne(Libro => Libro.autor).WithMany().HasForeignKey(Libro=>Libro.idAutor);

            builder.ToTable("Book", "dbo");
            builder.HasKey(Libro=>Libro.id);
            builder.HasMany(Libro=>Libro.bookAutores).WithOne().HasForeignKey(o=>o.idBook);

        }
    }
}
 