﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BookSocial2.Models;

namespace BookSocial2.Models
{
    public class book_autor
    {
        public int id { get; set; }
        public int idBook { get; set; }
        public int idAutor { get; set; }
        public Autor autor  {get;set;}
      
    }
}
