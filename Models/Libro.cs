﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookSocial2.Models
{
    public class Libro
    {
        public int id { set; get; }
        public string nombre { set; get; }
        public string portada { set; get; }
        public int idAutor   {set; get;}
        public List<book_autor> bookAutores { get; set; }

    }
}
