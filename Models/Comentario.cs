﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BookSocial2.Models
{
    public class Comentario
    {
        public int id { get; set; }
        public int idBook { get; set; }

        //features que nos ayudan a validar el contenido del atributo contenido
        [Required(ErrorMessage ="El campo contenido es requerido bro")]
        [MinLength(5,ErrorMessage ="Debe tener almenos 5 caracteres")]
        public string contenido { get; set; }
    }
}
