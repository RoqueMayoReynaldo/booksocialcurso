﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookSocial2.Models
{
    public class Autor
    {
        public int id { get; set; }
        public string nombre { get; set; }
        //public List<Libro> libros { get; set; }
    }
}
